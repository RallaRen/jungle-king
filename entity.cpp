#include "entity.h"

Entity::Entity(Sprite sprite)
{
	Entity::sprite = sprite;
}

void Entity::setHVel(int vel){
	hVel = vel;
}

int Entity::getHVel()
{
	return hVel;
}

int Entity::getVVel()
{
	return vVel;
}

Sprite& Entity::getSprite()
{
	return sprite;
}

