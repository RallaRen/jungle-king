#pragma once
#include "avancezlib.h"

class Entity {
	Sprite sprite;
	int hVel = -60;
	int vVel = 5;
public:
	Entity() = default;
	Entity(Sprite sprite);
	
	void setHVel(int vel); 
	
	int getHVel();
	int getVVel();
	Sprite& getSprite();
};