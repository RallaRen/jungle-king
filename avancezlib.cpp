#include "avancezlib.h"


	bool switcher = false;
	SDL_Color White = { 0, 0, 0 };
	void AvancezLib::destroy()
	{
		void TTF_Quit();
		TTF_CloseFont(font);
		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(window);
	}

	bool AvancezLib::init(int width, int height)
	{
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		{
			SDL_Log("SDL failed the initialization: %s\n", SDL_GetError());
		}
		//startTime = SDL_GetTicks();


		//Create window
		window = SDL_CreateWindow("Lab One", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			SDL_Log("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return false;
		}
		//Create renderer for window
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		if (renderer == NULL)
		{
			SDL_Log("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
			return false;
		}


		if (TTF_Init() < 0) {
			SDL_Log("ttf could not be init! SDL Error: %s\n", TTF_GetError());
		}
		int was_init = TTF_WasInit();
		//if (was_init == 1);
		// SDL_ttf was already initialized
		//else if (was_init == 0);

	
		font = TTF_OpenFont("space_invaders.ttf", 12);
		if (font == NULL)
		{
			SDL_Log("Font is null! SDL Error: %s\n", TTF_GetError());
		}

		//Initialize renderer color
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

		SDL_RenderClear(renderer);

		SDL_RenderPresent(renderer);
		
		return true;
	}

	bool AvancezLib::update()
	{
		bool done = false;
		SDL_Event event;
		key.up = false; key.left = false; key.right = false;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) 
			{
				done = true;
			}
			else if (event.type == SDL_KEYDOWN) 
			{
				switch (event.key.keysym.sym)
				{
					case SDLK_ESCAPE:
					case SDLK_q:
						done = true;
						break;
					case SDLK_UP:
						key.up = true;
						break;
					case SDLK_LEFT:
						key.left = true;
						break;
					case SDLK_RIGHT:
						key.right = true;
						break;
				}
			}
		}
		SDL_RenderPresent(renderer);
		SDL_RenderClear(renderer);
		return done;
	}
	
	

	void AvancezLib::drawText(int x, int y, const char * msg)
	{
		SDL_Surface* surfaceMessage = TTF_RenderText_Solid(font, msg, White); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

		SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

		SDL_Rect Message_rect; //create a rect
		Message_rect.x = x;  //controls the rect's x coordinate 
		Message_rect.y = y; // controls the rect's y coordinte
		Message_rect.w = 50; // controls the width of the rect
		Message_rect.h = 20; // controls the height of the rect

							  //Mind you that (0,0) is on the top left of the window/screen, think a rect as the text's box, that way it would be very simple to understance

							  //Now since it's a texture, you have to put RenderCopy in your game loop area, the area where the whole code executes

		SDL_RenderCopy(renderer, Message, NULL, &Message_rect);
		SDL_DestroyTexture(Message);
		SDL_FreeSurface(surfaceMessage);
	}

	int AvancezLib::getElapsedTime()
	{
		return (SDL_GetTicks());
	}

	void AvancezLib::getKeyStatus(KeyStatus & keys)
	{
		keys.up = key.up;
		keys.left = key.left;
		keys.right = key.right;
	}
	
	Sprite::Sprite(SDL_Renderer * renderer, SDL_Texture * texture)
	{
		this ->texture = texture;
		this ->renderer = renderer;
	}

	void Sprite::destroy()
	{
		SDL_DestroyTexture(texture);
	}

	void Sprite::draw()
	{
		SDL_QueryTexture(texture, NULL, NULL, &(rect.w), &(rect.h));
		SDL_RenderCopy(renderer, texture, NULL, &rect);
	}
	void Sprite::setXY(int x, int y)
	{
		rect.x = x;
		rect.y = y;
	}
	SDL_Point Sprite::getXY()
	{
		SDL_Point temp = { rect.x, rect.y };
		return temp;
	}
	Sprite  AvancezLib::createSprite(std::string  name)
	{
		SDL_Surface *image;    //This pointer will reference our bitmap sprite
		image = IMG_Load(name.data());
		SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, image);
		Sprite theSprite = Sprite(renderer, texture);

		SDL_FreeSurface(image);
		
		return theSprite;
	}
	SDL_Rect Sprite::getBox() {
		return rect;
	}
