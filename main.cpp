#include "SDL.h"
#include "avancezlib.h"
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include "entity.h"
using namespace std;
#define WIDTH	640
#define HEIGHT	840
#define JUMPHEIGHT	40
#define VERTICALVEL	5
#define HORIZONTALVEL	-60
#define MS_PER_UPDATE	16
//Declarations
bool handleMove(AvancezLib system);
vector <Sprite>  readMap(AvancezLib sys);
bool checkCollision(pair <int, int> direction);
//
// Variables
bool inJump = false;
bool falling = false;
Entity player = Entity();
vector <Sprite > map;
unsigned int previous;
unsigned int lag;
float nrFrames;
float avgfps;
//
int main(int argc, char* argv[])
{
	AvancezLib system;
	system.init(WIDTH,HEIGHT);
	
	
	player = Entity (system.createSprite("player.png"));
	player.getSprite().setXY(64, 8 * 64);
	map = readMap(system);

	previous = system.getElapsedTime();
	lag = 0;

	nrFrames = 0;
	avgfps = 0;
	while (true)
	{
		system.update();

		unsigned int currentTime = system.getElapsedTime();
		int elapsed = currentTime - previous;
		previous = currentTime;
		lag += elapsed;

		
		for (auto & var :map)
		{
			var.draw();
		}
		
		inJump = handleMove(system);
		player.getSprite().draw();
		if (lag > 100)
		{
			avgfps = (nrFrames / lag) * 1000;
			nrFrames = 0;
			lag = 0;
		}


		if ((system.getElapsedTime() - currentTime) <= MS_PER_UPDATE) {
			SDL_Delay(MS_PER_UPDATE - (system.getElapsedTime() - currentTime));
		}

		
	}

}


vector <Sprite>  readMap(AvancezLib sys)
{
	int nrline = 0;
	vector <Sprite> map;
	ifstream file("worldmap2.csv");
	string line;
	while (getline(file, line))
	{
		stringstream   linestream(line);
		string         value;
		int nrcol = 0;
		bool skip = false;
		while (getline(linestream, value, ','))
		{
			//std::cout << "Value(" << value << ")";
			string texture;
			switch (value.at(0))
			{
			case 'A':
				skip = true;
				break;
			case 'G':
				texture = "grass.png";
				break;
			case 'T':
				texture = "tree.png";
				break;
			}

			if (!skip)
			{
				Sprite temp = sys.createSprite(texture);
				temp.setXY(nrcol * 64, nrline * 64);
				map.push_back(temp);
				skip = false;
				nrcol++;
			}
		}

		++nrline;
	}

	return map;
}

bool handleMove(AvancezLib system) {

	pair<int, int> xy;
	bool moveLeft = false;
	bool moveRight = false;

	int jumpHeight = player.getHVel();
	AvancezLib::KeyStatus key;
	system.getKeyStatus(key);
	if (key.up)
	{
		if (!inJump) {
			inJump = true;
			falling = false;
			xy.second = player.getHVel();

		}
		SDL_Log("JUMP \n");
	}

	if (second)

	if (key.left) {
		SDL_Log("left! \n");
		moveLeft = true;

	}

	if (key.right) {
		SDL_Log("right! \n");
		moveRight = true;
	}

	if (inJump)
	{
		if (falling)
			xy.second += VERTICALVEL;
		else xy.second -= VERTICALVEL;

		//if above sprite is anything but air, inJump variable should be set to false; 	
	}
	else xy.second = 0;




	SDL_Rect temp = player.getSprite().getBox();
	bool collided = checkCollision(xy);
	// If xy.first == 0 whilst in a jump and not falling, a collision means that we collided
	// on the Y-axis, meaning we hit the ceiling.
	if ((collided && xy.first == 0) && !falling && inJump)
	{
		falling = true;
	}

	// If xy.second == 0 whilst in a jump and not falling, a collision means that we collided
	// on the X-axis, meaning we hit some object on the side, and should result in us falling.
	else if ((collided && xy.second == 0) && !falling && inJump)
	{
		falling = true;
	}


	// If xy.first == 0 whilst in a jump and falling, a collision means that we collided
	// on the Y-axis, meaning we hit the ground, and should result in us not falling and not in jump anymore.
	if ((collided && xy.first == 0) && falling && inJump)
	{
		inJump = false;
		falling = false;
		
	}
	
	// 2 collision checks
	// If xy.second and first is not  whilst in a jump and not falling, a collision means that we collided
	// on the X-axis, meaning we hit some object on the side, and should result in us falling.
	if ((collided &&(xy.first != 0) && xy.second != 0) && !falling && inJump)
	

	return false;
}


bool checkCollision(pair <int, int> direction)
{
	Sprite temp = player.getSprite();
	SDL_Rect box = temp.getBox();
	bool collided = false;
	
	box.x = box.x + direction.first;
	box.y = box.y + direction.second;

		//check player vs all sprites in map, se if anything in y direction will collide with player img
	for (auto sprite : map)
	{
		if (SDL_HasIntersection(&sprite.getBox(), &box))
		{ 
			collided = true;
			break;
		}
	}
	return collided;
}